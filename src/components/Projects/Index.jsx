import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
// component styles
import '../Projects/Index.scss';
import { Container, Row, Col } from 'react-bootstrap';
// data file
import jsonData from '../../data/projects.json';
import AOS from "aos";
import "aos/dist/aos.css";

const Projects = () => {
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  });

  const projectItems = jsonData.map((project) =>
    <Col key={ project.id } lg={ 4 } sm={ 6 }>
      <div className="work-projects__block work-projects--block" data-aos="fade-down">
        <img className="img-fluid" src={ project.image } alt={ project.name } />
        <div className="work-projects__detail">
          <div className="work-projects__title">{ project.name } | { project.stack[0] }</div>
          <div className="work-projects__buttons">
            <Link to={ `/project/${project.id}/view` }>View</Link>
            <a href={ project.bitbucketUrl }>Source Code</a>
          </div>
        </div>
      </div>
    </Col>
  );

  return (
    <div className="content bg">
      <Container>
        <h1 className="content-title">Projects</h1>
        <Row>
          <Col sm={ 12 }>
            <div className="work-projects">
              <Row>
                { projectItems }
              </Row>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Projects;