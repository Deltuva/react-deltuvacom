/* eslint-disable */
import React, { useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { useScrollToTop } from '../../../config/utils';
import AOS from "aos";
import "aos/dist/aos.css";
// component styles
import '../ItemView/Index.scss';
// data file
import jsonData from '../../../data/projects.json';
import { Link } from 'react-router-dom';

const ItemView = (props) => {
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  });

  const setScrollToTop = useScrollToTop(true);

  let projectId = parseInt(props.match.params.id);
  let foundProject = jsonData.find(projectObj => projectObj.id === projectId);

  const techStack = foundProject.stack.map((stack, index) => {
    return (
      <li key={index} className="tech-stack__item">{ stack }</li>
    );
  }
  );

  return (
    <div className="content bg">
      <Container>
        <h1 className="content-title">
          <Link to={ '/projects' }>Projects</Link> / { foundProject.name }
        </h1>
        <Row>
          <Col lg={ 6 }>
            <div className="work-project__view" data-aos="fade-right">
              <div className="work-project__screen">
                <img src={ foundProject.image } alt="" className="work-project__item-image img-fluid" />
              </div>
            </div>
          </Col>
          <Col lg={ 6 }>
            <div className="work-project__info" data-aos="fade-left">
              <h2 className="work-project__info-name">{ foundProject.name }</h2>
              <p className="work-project__info-description">{ foundProject.description }</p>
              <ul className="tech-stack">
                { techStack }
              </ul>
              <div className="work-project__info-buttons">
                <a href={ foundProject.liveUrl }>Live</a>
                <a href={ foundProject.bitbucketUrl }>Source Code</a>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default ItemView;