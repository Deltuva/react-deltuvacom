import React, { useState } from 'react';
import logo from '../../assets/logo.png';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';
import Hamburger from 'hamburger-react';
// component styles
import '../Header/Index.scss';

const Header = () => {
  const [isOpen, setOpen] = useState(false);
  const dateTimeNow = Date.now();

  return (
    <>
      <header className="main-header">
        <div className="main-header__logo">
          <Link to={ '/' }>
            <img src={ logo } alt="Logo" />
          </Link>
        </div>
        <nav>
          <ul className={ `main-header__nav ${isOpen ? "is-open" : ""}` } onMouseLeave={ () => setOpen(false) }>
            <Link to={ '/projects' }>
              <li className="main-header__nav-item">Works</li>
            </Link>
            <Link to={ '/about' }>
              <li className="main-header__nav-item">About</li>
            </Link>
            <Link to={ '/contact' }>
              <li className="main-header__nav-item">Contact</li>
            </Link>
            <li className="main-header__nav-item"><a href="/">
              <Moment
                date={ dateTimeNow }
                format="YYYY/MM/DD H:m:s"
              /></a></li>
          </ul>
        </nav>
        <Hamburger
          toggled={ isOpen }
          toggle={ setOpen }
        />
      </header>
    </>
  );
};

export default Header;