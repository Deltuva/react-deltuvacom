import React, { useEffect } from 'react';
import AOS from "aos";
import "aos/dist/aos.css";
// fontawesome imports
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// component styles
import './Index.scss';

const OfferAlert = () => {
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  });

  return (
    <div className="sended">
      <div className="sended__message" data-aos="zoom-in">
        <div className="sended__content">
          <FontAwesomeIcon
            data-aos="zoom-in"
            icon={ faCheck }
            size="2x"
          />
        </div>
        <div>{ `Thanks for your offer.` }</div>
      </div>
    </div>
  );
};

export default OfferAlert;