/* eslint-disable */
import React, { useState, useEffect } from 'react';
import { useForm } from "react-hook-form";
import { useScrollToTop, API_BACKEND_SEND_MAIL } from '../../config/utils';
import AOS from "aos";
import "aos/dist/aos.css";
// component styles
import '../Offer/Index.scss';
import { Container, Row, Col, Form, Button, Alert } from 'react-bootstrap';
import developmentImage from '../../assets/development.png';
import OfferAlert from './Alert';

const Offer = () => {
  const {
    register,
    errors,
    formState,
    reset,
    handleSubmit
  } = useForm();

  const setScrollToTop = useScrollToTop(true);
  const [isSuccessfullySubmitted, setIsSuccessfullySubmitted] = useState(false);
  const [isSendedLoader, setIsSendedLoader] = useState(false);

  const subjectTypes = [
    'Job offer',
    'Landing website',
    'E-commerce website',
    'Bug fixes',
    'Mobile application',
    'Other'
  ];

  useEffect(() => {
    AOS.init();
    AOS.refresh();
  });

  useEffect(() => {
    const alertMessage = () => setTimeout(() => {
      setIsSuccessfullySubmitted(false);
    }, 5000);
    const clear = alertMessage();

    return () => {
      clearTimeout(clear);
    };
  });

  const onOfferSend = async (data, e) => {
    e.preventDefault();
    setIsSendedLoader(true);

    const response = await fetch(API_BACKEND_SEND_MAIL, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(data)
    });
    const resData = await response.json();
    if (resData.status === 'success') {
      setIsSuccessfullySubmitted(true);
      setIsSendedLoader(false);
      e.target.reset();

    } else if (resData.status === 'fail') {
      console.log("Message failed to send.");
      setIsSuccessfullySubmitted(false);
      e.target.reset();
    }
  }

  return (
    <div className="content bg">
      { isSuccessfullySubmitted &&
        <OfferAlert />
      }
      <Container>
        <h1 className="content-title">{ `Offer Form` }</h1>
        <Row>
          <Col sm={ 6 }>
            <div className="offer-image" data-aos="fade-right">
              <img src={ developmentImage } alt="" className="img-fluid" />
            </div>
          </Col>
          <Col sm={ 6 }>
            <div className="offer-form" data-aos="fade-up">
              <Form
                onSubmit={ handleSubmit(onOfferSend) }
              >
                <Form.Group>
                  <Form.Label>{ `Your e-mail address:` }</Form.Label>
                  <Form.Control name="email" type="email" ref={ register({
                    required: true
                  }) } />
                  { errors.email && <Alert variant="danger">
                    { `E-Mail is required` }
                  </Alert> }

                </Form.Group>
                <Form.Group>
                  <Form.Label>{ `Subject type:` }</Form.Label>
                  <Form.Control as="select" name="subject" ref={ register({
                    required: true
                  }) }>
                    <option value="">{ `---` }</option>
                    { subjectTypes.map((subjectType, idx) =>
                      <option key={ idx } value={ subjectType }>
                        { subjectType }
                      </option>
                    ) }
                  </Form.Control>
                  { errors.subject && <Alert variant="danger">
                    { `Subject type is required` }
                  </Alert> }

                </Form.Group>
                <Form.Group>
                  <Form.Label>{ `Your comment:` }</Form.Label>
                  <Form.Control as="textarea" name="comment" rows="6" ref={ register({
                    required: true
                  }) } />
                  { errors.comment && <Alert variant="danger">
                    { `Comment is required` }
                  </Alert> }

                </Form.Group>
                {
                  !isSuccessfullySubmitted &&
                  <Button
                    variant="primary"
                    type="submit"
                    disabled={ formState.isSubmitting || isSuccessfullySubmitted }>
                    { isSendedLoader ? 'Sending…' : 'Send' }
                  </Button>
                }
              </Form>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Offer;