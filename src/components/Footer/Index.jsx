import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
// fontawesome imports
import { faMobileAlt, faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// component styles
import '../Footer/Index.scss';
import myPhoneNumber from '../../assets/mano_nr_footer.png';

const Footer = () => {
  return (
    <footer className="main-footer">
      <Container fluid>
        <Row>
          <Col className="block" lg={ 4 }>
            <div className="block__contact">
              <a href="/">
                <FontAwesomeIcon
                  icon={ faMobileAlt }
                /> <img src={myPhoneNumber} alt="Phone number"/></a>
            </div>
          </Col>
          <Col className="block" lg={ 4 }>
            <div className="block__contact">
              <a href="mailto:deltuva.mindaugas@gmail.com">
                <FontAwesomeIcon
                  icon={ faEnvelope }
                /> deltuva.mindaugas@gmail.com</a>
            </div>
          </Col>
          <Col className="block" lg={ 4 }>
            <div className="block__contact">
              <a href="/">skype: deltuva.mindaugas</a>
            </div>
          </Col>
        </Row>
      </Container>
    </footer>
  );
};

export default Footer;