/* eslint-disable */
import React, { useEffect } from 'react';
import { useScrollToTop } from '../../config/utils';
import AOS from "aos";
import "aos/dist/aos.css";
// component styles
import '../Contact/Index.scss';
import { Container, Row, Col } from 'react-bootstrap';
import myPhoneNumber from '../../assets/mano_nr.png';

const Contact = () => {
  const setScrollToTop = useScrollToTop(true);

  useEffect(() => {
    AOS.init();
    AOS.refresh();
  });

  return (
    <div className="content bg">
      <Container>
        <h1 className="content-title">Contact Us</h1>
        <Row>
          <Col sm={ 12 }>
            <div className="contact" data-aos="fade-up">
              <p><a href="/"><img src={myPhoneNumber} alt="Phone number"/></a></p>
              <p><a href="mailto:deltuva.mindaugas@gmail.com">deltuva.mindaugas@gmail.com</a></p>
              <p><a href="/">skype: deltuva.mindaugas</a></p>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Contact;