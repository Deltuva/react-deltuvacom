import React from 'react';
// fontawesome imports
import { faHandshake } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from 'react-router-dom';

// component styles
import './Index.scss';

const OfferButton = () => {
  return (
    <div className="offer">
      <div className="offer__btns">
        <Link to={ `/offer` }>
          <button className="offer__btns-btn offer__btns-btn--darkblue">
            <FontAwesomeIcon
              icon={ faHandshake }
            />
          </button>
        </Link>
      </div>
    </div>
  );
};

export default OfferButton;