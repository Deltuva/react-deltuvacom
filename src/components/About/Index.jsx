/* eslint-disable */
import React, { useEffect } from 'react';
import { useScrollToTop } from '../../config/utils';
import AOS from "aos";
import "aos/dist/aos.css";
// component styles
import '../About/Index.scss';
import { Container, Row, Col } from 'react-bootstrap';
import myAvatar from '../../assets/avatar/my.jpg';

const About = () => {
  const setScrollToTop = useScrollToTop(true);

  useEffect(() => {
    AOS.init();
    AOS.refresh();
  });

  return (
    <div className="content bg">
      <Container>
        <h1 className="content-title">About Me</h1>
        <Row>
          <Col sm={ 12 }>
            <div className="about" data-aos="fade-up">
              <img src={myAvatar} className="rounded-circle " alt="..." />
              <p>Hello My Name Is Mindaugas<br /> I'm Self-taught Web developer. I'm interested in all kinds of development. I also have skills in other fields like back-end web development Laravel, Symfony, NodeJs.</p>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default About;