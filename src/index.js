import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './components/Header/Index';
import Offer from './components/Offer/Index';
import OfferButton from './components/Partials/OfferButton/Index';
import Footer from './components/Footer/Index';
import Home from './components/Home/Index';
import Technologies from './components/Technologies/Index';
import Projects from './components/Projects/Index';
import ProjectsItemView from './components/Projects/ItemView/Index';
import About from './components/About/Index';
import Contact from './components/Contact/Index';
import Page404 from './404/Index';
// common styles
import './styles/common.scss';

ReactDOM.render(
  <Router>
    <Header />
    <OfferButton />
    <Switch>
      <Route path="/" exact={ true } component={ Home } />
      <Route path="/offer" component={ Offer } />
      <Route path="/technologies" component={ Technologies } />
      <Route path="/projects" component={ Projects } />
      <Route path="/project/:id/view" exact={ true } component={ ProjectsItemView } />
      <Route path="/about" component={ About } />
      <Route path="/contact" component={ Contact } />
      <Route component={ Page404 } />
    </Switch>
    <Footer />
  </Router>,
  document.getElementById('root')
);