import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
// component styles
import '../404/Index.scss';

const NotFound = () => {
  return (
    <div className="content bg">
      <Container>
        <Row>
          <Col sm={ 12 }>
            <div className="not-found">
              <h1>404</h1>
              <p>Page Not Found.</p>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default NotFound;