import express from 'express';
import path from 'path';
import cors from 'cors';
import nodemailer from 'nodemailer';

let app = express();
app.use(express.json());
app.use(cors());

let transporter = nodemailer.createTransport({
  host: "smtp.mailtrap.io",
  port: 2525,
  auth: {
    user: "6d74e702cf4d2e",
    pass: "c8545e5e5ca19f"
  }
});

app.use('/public', express.static(path.join(path.resolve(), 'public')));

transporter.verify(function (error, success) {
  if (error) {
    console.log(error);
  } else {
    console.log("Server is ready to take our messages!");
  }
});

app.post('/access', (req, res, next) => {
  let email = req.body.email;
  let subject = req.body.subject;
  let comment = req.body.comment;

  let mail = {
    from: email,
    to: email,
    subject: subject,
    text: comment
  }

  transporter.sendMail(mail, (err, data) => {
    if (err) {
      res.json({
        status: 'fail'
      })
    } else {
      res.json({
        status: 'success'
      })
    }
  })
})


const PORT = process.env.PORT || 8080;
app.listen(PORT, () => console.info(`server has started on ${PORT}`));